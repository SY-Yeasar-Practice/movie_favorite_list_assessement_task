import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FavoriteList, FavoriteListDocument } from 'src/model/favouriteList.schema';
import { UserDocument,User } from 'src/model/user.schema';

@Injectable()
export class UserService {
    constructor(
        @InjectModel(User.name) private User: Model<UserDocument>,
        @InjectModel (FavoriteList.name) private FavoriteList: Model <FavoriteListDocument>
    ) {}
    
}

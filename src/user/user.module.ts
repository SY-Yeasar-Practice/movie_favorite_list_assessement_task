import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from 'src/model/user.schema';
import { FavoriteList, FavoriteListSchema } from 'src/model/favouriteList.schema';

@Module({
  imports:[
    MongooseModule.forFeature(
      [
        {
          name: User.name, 
          schema: UserSchema
        },
        {
          name: FavoriteList.name, 
          schema: FavoriteListSchema
        }
      ]
    )
  ],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {}

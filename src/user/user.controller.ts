import { Body, Controller, Get, Req, Res } from '@nestjs/common';
import { UserService } from './user.service';
import {Request, Response} from "express"

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get ("/")
  async getUser(@Req() req: Request, @Res() res: Response) : Promise<void> {
    
  }
}

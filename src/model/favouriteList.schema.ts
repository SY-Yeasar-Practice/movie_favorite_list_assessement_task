import {
    SchemaFactory,
    MongooseModule,
    Schema,
    Prop
} from "@nestjs/mongoose"
import {
    UserDocument,
    User
} from "./user.schema"
import mongoose, {
    Document
} from "mongoose"

export type FavoriteListDocument = FavoriteList & Document

@Schema ({
    timestamps: true
})
export class FavoriteList {
    @Prop ({
        type: String,
        required: true
    }) 
    slug: string
    
    @Prop({
        type: String,
        required: true
    })
    movieName: string
    
    @Prop({
        type: Boolean,
        default: false
    })
    isDelete: boolean
    
    @Prop({
        type: Date,
        default: Date.now()
    })
    releasedDate: Object 

    @Prop({
        type: String,
        required: true
    })
    imDbId : string

    @Prop ({
        type: String,
        required: true
    })
    movieCategory: string

    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        ref: `${User.name}`
    })
    listedBy: UserDocument
}

export const FavoriteListSchema = SchemaFactory.createForClass (FavoriteList)
import {
    Prop,
    Schema,
    SchemaFactory
} from "@nestjs/mongoose"

import { Document } from "mongoose"

export type UserDocument = User & Document

@Schema ({
    timestamps: true
})
export class User {
    @Prop ({
        type: String,
        required: true
    })
    slug: string

    @Prop ({
        type: String,
        required: true,
        max: 5,
        min: 5
    })
    userId: string

    @Prop ({
        required: true,
        type: String,
    })
    firstName: string

    @Prop ({
        required: true,
        type: String,
        
    })
    lastName: string

    @Prop ({
        required: true,
        type: String
    })
    profilePicture: string

    @Prop ({
        required: true,
        type: String,
        enum: ["male", "female"]
    })
    gender : string

    @Prop({
        type: String,
        required: true
    })
    userType: string

    @Prop({
        unique: true,
        required: true,
        type: String
    })
    email: string

    @Prop({
        required: true,
        type: String
    })
    password: string

    @Prop({
        type: Boolean,
        default: false
    })
    isDelete: boolean

    @Prop({
        type: Boolean,
        default: true
    })
    isActive: boolean

    @Prop ({
        typs: String,
        max: 4,
        min: 4
    })
    otp: string
}

export const UserSchema = SchemaFactory.createForClass(User)
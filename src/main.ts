import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from "express"
import {config} from "dotenv"
import * as cookieParser from 'cookie-parser';
import * as cors from "cors"

config();


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const PORT:string | number = process.env.PORT || 8080
  await app.listen(PORT, () => console.log (`Server is connected to ${PORT}`));
  app.use (express.json({limit: "250mb"}))
  app.use (express.urlencoded({extended: true, limit: "250mb"}))
  app.use (cookieParser())
  app.use (cors ())
}
bootstrap();
